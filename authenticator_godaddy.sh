#!/bin/bash

# source values in the config file
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
source $SCRIPT_DIR/config

# If certbot did not supply values, default to the configured test values
CERTBOT_DOMAIN="${CERTBOT_DOMAIN:-${TEST_CERTBOT_DOMAIN}}"
CERTBOT_VALIDATION="${CERTBOT_VALIDATION:-${TEST_CERTBOT_VALIDATION}}"

# DNS entry propagation parameters
# Delay between the DNS record update and the first dig request (in seconds)
DELAY_AFTER_DNS_RECORD_UPDATE=10
# Time interval between each dig request (in seconds)
DIG_TIME_INTERVAL=4
# Number of retries of dig request before ending in a failure
DIG_NB_RETRIES=35

# Init variables
DOMAIN=""
SUBDOMAIN=""

LOG_FILE="$LOG_DIR/authenticator.$CERTBOT_DOMAIN.log"

echo "" > $LOG_FILE

function log {
  DATE=$(date)
  echo "$DATE: ${1}" >> $LOG_FILE
  echo "$DATE: ${1}"
}

function fqdn_from_domain {
  local fqdn="$(expr "$1" : '.*\.\(.*\..*\)')"
  if test -z $fqdn; then
    fqdn="$1"
  fi
  echo $fqdn
}

function subdomain_from_domain {
  local fqdn="$(fqdn_from_domain "$1")"
  echo "${1%.${fqdn}}"
}

log "[BEGIN]"

DOMAIN="$(fqdn_from_domain $CERTBOT_DOMAIN)"
SUBDOMAIN="$(subdomain_from_domain $CERTBOT_DOMAIN)"

log "DOMAIN $DOMAIN"
log "SUBDOMAIN $SUBDOMAIN"

# Update TXT record
RECORD_TYPE="TXT"


if [[ ! -z "${SUBDOMAIN}" ]]
then
  RECORD_NAME="_acme-challenge.$SUBDOMAIN"
else
  RECORD_NAME="_acme-challenge"
fi
# Technically the nameserver can be different for each subdomain,
# but in our case we don't change the default nameserver
NS=$(dig @208.67.222.222 $DOMAIN NS +short | tail -1)

log "RECORD_NAME $RECORD_NAME"
log "NS $NS"

DEFAULT_CERTBOT_VALIDATION="default_value"

log "CERTBOT VALIDATION $CERTBOT_VALIDATION"
log "CERTBOT DOMAIN $CERTBOT_DOMAIN"

if [ -z $CERTBOT_VALIDATION ]
then
  log "CERTBOT_VALIDATION is unset"
  eval CERTBOT_VALIDATION=$DEFAULT_CERTBOT_VALIDATION
  log "CERTBOT_VALIDATION has been set to $CERTBOT_VALIDATION"
else
  log "CERTBOT_VALIDATION is set to '$CERTBOT_VALIDATION'"
fi


# Update the previous record
IS_NONE=$(dig @$NS $RECORD_NAME.$DOMAIN TXT +short | grep -e "none")
if [ $? -eq 0 ]
then
  # PUT the record
  RESPONSE_CODE=$(curl -s -X PUT -w %{http_code} \
  -H "Authorization: sso-key $API_KEY:$API_SECRET" \
  -H "Content-Type: application/json" \
  -d "[{\"data\": \"$CERTBOT_VALIDATION\", \"ttl\": 600}]" \
  "https://api.godaddy.com/v1/domains/$DOMAIN/records/$RECORD_TYPE/$RECORD_NAME")
else
  # PATCH the existing record (for wildcard / SAN certificates)
  RESPONSE_CODE=$(curl -s --request PATCH -w %{http_code} \
  -H "Authorization: sso-key $API_KEY:$API_SECRET" \
  -H "Content-Type: application/json" \
  -d "[{\"data\": \"$CERTBOT_VALIDATION\", \"name\": \"$RECORD_NAME\", \"type\": \"$RECORD_TYPE\", \"ttl\": 600}]" \
  "https://api.godaddy.com/v1/domains/$DOMAIN/records")
fi


if [ "$RESPONSE_CODE" == "200" ]
then
  log "OK"
  sleep $DELAY_AFTER_DNS_RECORD_UPDATE
  I=0
  while [ $I -le $DIG_NB_RETRIES ]
  do
    sleep $DIG_TIME_INTERVAL
    R=$(dig +short @$NS $RECORD_NAME.$DOMAIN txt | grep -e "$CERTBOT_VALIDATION")
    if [ $? -eq 0 ]
    then
      log "TEST $I > TOKEN FOUND"
      # Give it some time before letting certbot try to find the record
      #
      sleep $DELAY_AFTER_DNS_RECORD_UPDATE
      break
    else
      log "TEST $I > TOKEN NOT FOUND"
      let I++
    fi
  done
else
  log "KO"
  log "${RESPONSE_CODE}"
fi

log "[END]"
