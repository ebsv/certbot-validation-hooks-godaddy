#!/bin/bash

# source values in the config file
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
source $SCRIPT_DIR/config

# Init variables
DOMAIN=""
SUBDOMAIN=""

# If certbot did not supply values, default to the configured test values
CERTBOT_DOMAIN="${CERTBOT_DOMAIN:-${TEST_CERTBOT_DOMAIN}}"
CERTBOT_VALIDATION="${CERTBOT_VALIDATION:-${TEST_CERTBOT_VALIDATION}}"

LOG_FILE="$LOG_DIR/cleanup.$CERTBOT_DOMAIN.log"

echo "" > $LOG_FILE

function log {
  DATE=$(date)
  echo "$DATE: $1" >> $LOG_FILE
}

function fqdn_from_domain {
  local fqdn="$(expr "$1" : '.*\.\(.*\..*\)')"
  if test -z $fqdn; then
    fqdn="$1"
  fi
  echo $fqdn
}

function subdomain_from_domain {
  local fqdn="$(fqdn_from_domain "$1")"
  echo "${1%.${fqdn}}"
}

log "[BEGIN]"

DOMAIN="$(fqdn_from_domain $CERTBOT_DOMAIN)"
SUBDOMAIN="$(subdomain_from_domain $CERTBOT_DOMAIN)"

log "DOMAIN $DOMAIN"
log "SUBDOMAIN $SUBDOMAIN"

# Update TXT record
RECORD_TYPE="TXT"
RECORD_VALUE="none"

if [[ ! -z "${SUBDOMAIN// }" ]]
then
  RECORD_NAME="_acme-challenge.$SUBDOMAIN"
else
  RECORD_NAME="_acme-challenge"
fi

log "RECORD_NAME $RECORD_NAME"

# Update the previous record to default value
RESPONSE_CODE=$(curl -s -X PUT -w %{http_code} \
-H "Authorization: sso-key $API_KEY:$API_SECRET" \
-H "Content-Type: application/json" \
-d "[{\"data\": \"$RECORD_VALUE\", \"ttl\": 600}]" \
"https://api.godaddy.com/v1/domains/$DOMAIN/records/$RECORD_TYPE/$RECORD_NAME")

if [ "$RESPONSE_CODE" == "200" ]
then
  log "OK"
else
  log "KO"
  log $RESPONSE_CODE
fi

log "[END]"
